# Description

This project demonstrates how to use MySQL as the Identity database in an ASP.NET Core MVC project.
It is the result of following the steps in the Script section below.  

## Prerequisites

To build the application from scratch, you will first need the following.
1.  The dotnet command line (CLI) tool
2.  The MySQL database server.<br>
I recommend using the Community edition from Oracle instead of MariaDB because there are some differences in the maximum length of string keys.  The script below contains some steps to handle long keys that will normally
break MySQL, including Oracle's MySQL.

# Script

## 1. Create the project
```
$ dotnet new mvc --auth Individual --name MySite
```

## 2. Change MySite.csproj to use MySQL
```
In MySite.csproj:

From: <PackageReference Include="Microsoft.EntityFrameworkCore.Sqlite" Version="2.2.4" />
To:   <PackageReference Include="MySql.Data.EntityFrameworkCore" Version="8.0.18" />
```
# 3. Change connection string
```
In appsettings.json:

"DefaultConnection": "server=localhost;database=mysite;uid=mysite;pwd=mysite;pooling=true;"
```
# 4. Create the ApplicationUser class
```
Create Models/ApplicationUser.cs:

using Microsoft.AspNetCore.Identity;
namespace MySite.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Roles { get; set; }
        public int SiteID { get; set; }
    }
}
```

# 5. Delete the Migrations directory
```
$ rmdir -rf Data/Migrations
```

# 6. Update Startup.cs to user MySQL for Identity database
```
Update Startup.cs

From: 
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(
                    Configuration.GetConnectionString("DefaultConnection")));
To:
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseMySQL(Configuration.GetConnectionString("DefaultConnection"))
            );
```
# 7. Update Data/ApplicationDbContext.cs
## From
```
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MySite.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
```
## To
```
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MySite.Models;

namespace MySite.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>  options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //----------------------------------------------------------------------
            // Modify identity entities to have shorter keys to circumvent 
            // MySQL long string key errors
            //----------------------------------------------------------------------

            builder.Entity<ApplicationUser>(entity => entity.Property(m =>  m.Id).HasMaxLength(85));
            builder.Entity<ApplicationUser>(entity => entity.Property(m =>  m.NormalizedEmail).HasMaxLength(85));
            builder.Entity<ApplicationUser>(entity => entity.Property(m =>  m.NormalizedUserName).HasMaxLength(85));
            builder.Entity<IdentityRole>(entity => entity.Property(m =>  m.Id).HasMaxLength(85));
            builder.Entity<IdentityRole>(entity => entity.Property(m =>  m.NormalizedName).HasMaxLength(85));
            builder.Entity<IdentityUserLogin<string>>(entity => entity.Property(m  => m.LoginProvider).HasMaxLength(85));
            builder.Entity<IdentityUserLogin<string>>(entity => entity.Property(m  => m.ProviderKey).HasMaxLength(85));
            builder.Entity<IdentityUserLogin<string>>(entity => entity.Property(m  => m.UserId).HasMaxLength(85));
            builder.Entity<IdentityUserLogin<string>>(entity => entity.Property(m  => m.UserId).HasMaxLength(85));
            //builder.Entity<IdentityUserLogin<string>>(entity =>  entity.Property(m => m.RoleId).HasMaxLength(85));
            builder.Entity<IdentityUserToken<string>>(entity => entity.Property(m  => m.UserId).HasMaxLength(85));
            builder.Entity<IdentityUserToken<string>>(entity => entity.Property(m  => m.LoginProvider).HasMaxLength(85));
            builder.Entity<IdentityUserToken<string>>(entity => entity.Property(m  => m.Name).HasMaxLength(85));
            builder.Entity<IdentityUserClaim<string>>(entity => entity.Property(m  => m.Id).HasMaxLength(85));
            builder.Entity<IdentityUserClaim<string>>(entity => entity.Property(m  => m.UserId).HasMaxLength(85));
            builder.Entity<IdentityRoleClaim<string>>(entity => entity.Property(m  => m.Id).HasMaxLength(85));
            builder.Entity<IdentityRoleClaim<string>>(entity => entity.Property(m  => m.RoleId).HasMaxLength(85));
            
            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                foreach (var property in entityType.GetProperties())
                {
                    if (property.ClrType == typeof(bool))
                    {
                        property.SetValueConverter(new BoolToIntConverter());
                    }
                }
            }
        }
    }

    //----------------------------------------------------------------------
    //  
    // BoolToIntConverter class referenced above
    //
    //----------------------------------------------------------------------
    public class BoolToIntConverter : ValueConverter<bool, int>
    {
        public BoolToIntConverter(ConverterMappingHints mappingHints = null)
            : base(
                  v => Convert.ToInt32(v),
                  v => Convert.ToBoolean(v),
                  mappingHints)
        {
        }
        public static ValueConverterInfo DefaultInfo { get; }
            = new ValueConverterInfo(typeof(bool), typeof(int), i => new  BoolToIntConverter(i.MappingHints));
    }
}
```
# 8. Create the MySQL database and acount
```
$ mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 26
Server version: 8.0.13 MySQL Community Server - GPL

Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database mysite;
Query OK, 1 row affected (0.07 sec)

mysql> create user 'mysite'@'localhost' identified by 'mysite';
Query OK, 0 rows affected (0.07 sec)

mysql> grant all privileges on mysite.* to 'mysite'@'localhost';
Query OK, 0 rows affected (0.03 sec)


#######################################################################
#
# Create the __EFMigrationsHistory table
#
#######################################################################

mysql> show tables;
Empty set (0.00 sec)

mysql> CREATE TABLE __EFMigrationsHistory ( MigrationId nvarchar(150) NOT NULL, ProductVersion nvarchar(32) NOT NULL, PRIMARY KEY (MigrationId) );
Query OK, 0 rows affected, 2 warnings (0.05 sec)

mysql> quit
```

# 9. Initialize the database
```
$ dotnet ef migrations add 000-InitialMigration
$ mysql -u mysite -pmysite mysite -e "show tables"
mysql: [Warning] Using a password on the command line interface can be insecure.
+-----------------------+
| Tables_in_mysite      |
+-----------------------+
| __EFMigrationsHistory |
| AspNetRoleClaims      |
| AspNetRoles           |
| AspNetUserClaims      |
| AspNetUserLogins      |
| AspNetUserRoles       |
| AspNetUsers           |
| AspNetUserTokens      |
+-----------------------+
```

# 10. Test the site
```
$ dotnet run
```

Browse to http://localhost:5000 and Register a new account.
