using Microsoft.AspNetCore.Identity;
namespace MySite.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Roles { get; set; }
        public int SiteID { get; set; }
    }
}